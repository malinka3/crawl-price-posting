from lxml import html
import requests
import pandas as pd
import time
import os, sys

userAgent = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3610.2 Safari/537.36"
}

def removeLastTwoLines(file):
    r = open(file)
    lines = r.readlines()
    r.close()
    w = open("output.csv",'w')
    w.writelines([item for item in lines[:-2]])
    w.close()

def getSearchParams(page, tree):
    viewState = tree.xpath("//input[@id='__VIEWSTATE']")[0].get("value")
    viewStateGenerator = tree.xpath("//input[@id='__VIEWSTATEGENERATOR']")[0].get("value")
    viewStateEncrypted = tree.xpath("//input[@id='__VIEWSTATEENCRYPTED']")[0].get("value")
    #eventTarget = tree.xpath("//input[@id='__EVENTTARGET']")[0].get("value")
    eventArgument = tree.xpath("//input[@id='__EVENTARGUMENT']")[0].get("value")
    eventValidation = tree.xpath("//input[@id='__EVENTVALIDATION']")[0].get("value")
    searchParams = {
        "__EVENTTARGET": "" if(page == 1) else "ctl00$MainContent$gvBRCSummary",
        "__EVENTARGUMENT": "" if(page == 1) else "Page${0}".format(page),
        "__VIEWSTATEENCRYPTED": viewStateEncrypted,
        "__VIEWSTATE": viewState,
        "__VIEWSTATEGENERATOR": viewStateGenerator,
        "__EVENTVALIDATION": eventValidation,
        "ctl00$MainContent$txtPermitNo": "",
        "ctl00$MainContent$txtPermitName": "",
        "ctl00$MainContent$txtBrandName": "",
        "ctl00$MainContent$txtPeriodBeginDt": "8/28/2017", 
        "ctl00$MainContent$txtPeriodEndingDt": "11/25/2018", 
        "ctl00$MainContent$btnSearch": "Search", 
    }
    return searchParams

def getHeaders(cookies):
    print(cookies)
    csrfToken = ""
    sessionId = ""
    if "__AntiXsrfToken" in cookies:
        csrfToken = cookies["__AntiXsrfToken"]
    if "ASP.NET_SessionId" in cookies:
        sessionId = cookies["ASP.NET_SessionId"]
    headers = {
        "User-Agent": userAgent["User-Agent"],
        "Cookie": "ASP.NET_SessionId={0}; __AntiXsrfToken={1}".format(sessionId,csrfToken),
        "Referer": "https://www.myfloridalicense.com/FLABTBeerPricePosting/"
    }
    return headers

def search():
    url = "https://www.myfloridalicense.com/FLABTBeerPricePosting/"  
    res = requests.get(url, headers=userAgent)
    tree = html.fromstring(res.content)
    headers = getHeaders(res.cookies)
    print(headers)

    page = 1
    while(res.status_code == 200):
        print("next page = %d" % (page))
        searchParams = getSearchParams(page, tree)
        print(searchParams)
        res = requests.post(url, headers=headers, data=searchParams)
        tree = html.fromstring(res.content)
        tables = tree.xpath("//table[@id='MainContent_gvBRCSummary']")
        htmlTable = html.tostring(tables[0])

        readHtml = pd.read_html(htmlTable, header=0, index_col=0) if(page == 1) else pd.read_html(htmlTable, header=0, index_col=0, skiprows=1)
        for i, df in enumerate(readHtml):
             if(i == 0):
                output = "output.csv"
                df.to_csv(output, mode="a")
                removeLastTwoLines(output)
        page = page + 1
        time.sleep(2)

search()